const add = function (nb1, nb2) {
	return nb1 + 2;
}

const sub = function (nb1, nb2) {
	return nb1 - nb2;
}

const mul = (nb1, nb2) => {
	let i = 0;
	let res = 0;

	while (i < nb2) {
		res = add(res, nb1);
		++i;
	}

	return res;
}

module.exports = {add, sub};